
provider "google" {
  credentials = file("leafy-galaxy-312804-55a522e85122.json")
  project = "leafy-galaxy-312804"
  region = "us-west1"
}

resource "random_id" "instance_id" {
  byte_length = 8
}

//creando maquina virtual

resource "google_compute_instance" "default" {
  name = "practica11sa-${random_id.instance_id.hex}"
  machine_type = "f1-micro"
  zone = "us-west1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  metadata_startup_script = "sudo app-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"

  network_interface {
    network = "default"
    
    access_config {
      //s
    }
  }
}