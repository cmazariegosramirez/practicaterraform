
provider "google" {
  credentials = file("leafy-galaxy-312804-55a522e85122.json")
  project = "leafy-galaxy-312804"
  region = "us-west1"
  zone = "us-west1-a"
}

resource "google_compute_instance" "default" {
    can_ip_forward          = false
    deletion_protection     = false
    enable_display          = false
    guest_accelerator       = []
    labels                  = {}
    machine_type            = "f1-micro"
    metadata                = {}
    metadata_startup_script = "sudo app-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"
    name                    = "practica12sa"
    project                 = "leafy-galaxy-312804"
    resource_policies       = []
    tags                    = []
    zone                    = "us-west1-a"

    boot_disk {

        initialize_params {
            image  = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-9-stretch-v20210420"
            labels = {}
            size   = 10
            type   = "pd-standard"
        }
    }

    network_interface {
        network            = "https://www.googleapis.com/compute/v1/projects/leafy-galaxy-312804/global/networks/default"
        access_config {
            //kjh
        }
    }

    scheduling {
        automatic_restart   = true
        min_node_cpus       = 0
        on_host_maintenance = "MIGRATE"
        preemptible         = false
    }

    timeouts {}
}


